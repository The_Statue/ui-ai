﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    GameObject[] characters;
    int turn = 0;

    public CameraManager cameraManager;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        characters = new GameObject[(enemies.Length + 1)];

        characters[0] = GameObject.FindGameObjectWithTag("Player"); //Player in position 0
        for (int i = 0; i < enemies.Length; i++) //enemies in position 1 to enemies.Length
            characters[i + 1] = enemies[i];

        cameraManager.UpdateTarget(characters[0]);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void IncrementTurn()
    {
        if (turn+1 < (characters.Length*2))
        {
            turn++; 
        }
        else
        {
            Debug.Log("Turn cycled to start!");
            turn = 0;
        }

        if ((int)turn / 2 == (float)turn / 2) //Update the camera with a new target
        {
            cameraManager.UpdateTarget(characters[turn / 2]);
        }

        Debug.Log("Turn is now: " + turn + " " + characters.Length*2 );
    }
    public void SkipTurn()
    {
        IncrementTurn();
        IncrementTurn();
    }

    public bool CheckTurn(GameObject character)
    {
        if ((int)turn / 2 != (float)turn / 2)
            return false;
        if (characters[turn / 2] == character)
            return true;
        return false;
    }
}
