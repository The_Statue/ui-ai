﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool walkable;
    public Vector3 worldPosition;
    public Node parent;
    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;

    public int fCost
    {
        get { return gCost + hCost; }
    }

    // Start is called before the first frame update
    public Node (bool walk, Vector3 worldPos, int X, int Y)
    {
        walkable = walk;
        worldPosition = worldPos;
        gridX = X;
        gridY = Y;
    }
}
