﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 0;
    float tempHealth = 0;
    short maxHealth = 10;
    short defence = 5;
    short attack = 5;
    short movement = 5;

    bool dead = false;

    short state = 0;

    GameObject player;
    GameObject pointer;
    Vector3 ghostPlayer;
    Vector3 spawn;

    public Path pathfinder;
    float timer = 0;
    float animationSpeed = 10;
    Vector3 originalPos;
    List<Node> path;

    public TurnManager turnManager;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pointer = transform.GetChild(0).gameObject;

        spawn = gameObject.transform.position;
        ghostPlayer = spawn;

        originalPos = Vector3.zero;
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(dead)
        {
            if (turnManager.CheckTurn(gameObject))
                turnManager.SkipTurn();
            GetComponent<SpriteRenderer>().enabled = false;
            pointer.SetActive(false);
            return; //Being dead requires nothing, kept active due to references
        }

        findPath();

        if (LineOfSight())
        {
            if (ghostPlayer == spawn)
                Debug.Log(name + " has line of sight!");
            ghostPlayer = player.transform.position;
        }

        pointer.transform.position = ghostPlayer;

        //pathfinding animation
        if (path != null && path.Count > 0)
        {
            if (originalPos == Vector3.zero)
            {
                originalPos = gameObject.transform.position;
                timer = 0;
            }
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, path[0].worldPosition, 0.01f);
            if (Vector3.Distance(gameObject.transform.position, path[0].worldPosition) <= 0) //Reached end of node
            {
                path.RemoveAt(0);
                originalPos = Vector3.zero;
                if (path.Count == 1 && Vector3.Distance(path[0].worldPosition, player.transform.position) <= 0.2f)
                {
                    path.RemoveAt(0);
                    player.GetComponent<Player>().Damage(attack);
                }

                if (path.Count == 0)
                    turnManager.IncrementTurn();
                //else
                //path.RemoveAt(0);
            }
        }
        if (originalPos != Vector3.zero)
        {
            gameObject.transform.position = Vector3.Lerp(originalPos, path[0].worldPosition, timer);
            //Debug.Log("Pos: " + gameObject.transform.position + " " + originalPos + " " + path[0].worldPosition);
        }
        timer += Time.deltaTime * animationSpeed;
    }

    void findPath()
    {
        //Pathfinding
        if (pathfinder != null && (path == null || path.Count == 0) && turnManager.CheckTurn(gameObject)) //TODO: add state condition
        {
            switch (state)
            {
                case 0: //Idle state, check for raycast and call it a day

                    if (ghostPlayer != spawn && Vector3.Distance(ghostPlayer, gameObject.transform.position) > 0.1f)
                    {
                        Debug.Log(name + " saw the player!");
                        state = 1;
                        break;
                    }

                    //if no line of sight, move to spawn
                    Debug.Log(name + " is going to spawn");
                    //turnManager.SkipTurn();
                    break;
                case 1:
                    if (Vector3.Distance(ghostPlayer, gameObject.transform.position) > 0.5f || LineOfSight())
                    {
                        Debug.Log(name + " is attempting to move towards player");
                    }
                    else
                    {
                        Debug.Log(name + " has lost line of sight!");
                        ghostPlayer = spawn;
                        state = 0;
                    }
                    break;
                default:
                    Debug.LogWarning(name + " is outside of state machine!");
                    //turnManager.IncrementTurn(); //skip turn if outside of statemachine
                    return;
            }

            path = pathfinder.FindPath(gameObject.transform.position, ghostPlayer);

            if (path.Count > movement)
            {
                Debug.Log(name + " attempted to move more than they can");
                path.RemoveRange(movement, path.Count - movement);
            }

            if (path.Count > 0)
            {
                //Wait and change to next turn/agent
                turnManager.IncrementTurn();
                Debug.Log(name + " New Path Set!");
            }
            else
            {
                Debug.Log("Pos " + gameObject.transform.position + " " + ghostPlayer);
                Debug.Log(name + " No path");
                turnManager.SkipTurn();
            }
            if (path.Count > movement) //null turn if it is illegal
            {
                Debug.LogError(gameObject.name + " Illegal move", gameObject);
                path.Clear();
            }
        }
    }

    bool LineOfSight()
    {
        //Debug.Log("AI checking for line of sight");
        Vector3 direction = (player.transform.position - gameObject.transform.position).normalized;
        RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, direction);

        if (hit.collider != null && hit.collider.gameObject == player)
            return true;
        return false;
    }

    public void Damage(short modifier)
    {
        modifier *= 2;
        modifier -= defence;
        if (modifier <= 0)
            return;

        Debug.Log(name + " took " + modifier + " damage");
        health -= modifier;
        if (health < 0)
            dead = true;
    }
}
