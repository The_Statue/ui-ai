﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    GameObject player;
    Player playerHandler;
    Image healthBar;

    Text defenceCount;
    Text attackCount;
    Text movementCount;
    Text pointsCount;

    GameObject pauseMenu;
    GameObject playerMenu;

    bool isPauseMenu = false;
    bool isPlayerMenu = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHandler = player.transform.GetComponent<Player>();
        healthBar = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>();

        pauseMenu = gameObject.transform.GetChild(2).gameObject;
        playerMenu = gameObject.transform.GetChild(3).gameObject;

        pauseMenu.SetActive(false);
        playerMenu.SetActive(false);

        defenceCount = playerMenu.transform.GetChild(0).GetChild(2).GetComponent<Text>();
        attackCount = playerMenu.transform.GetChild(1).GetChild(2).GetComponent<Text>();
        movementCount = playerMenu.transform.GetChild(2).GetChild(2).GetComponent<Text>();
        pointsCount = playerMenu.transform.GetChild(3).GetComponent<Text>();

        ModifyAttack(5);
        ModifyDefence(5);
        ModifyMovement(5);

        gameObject.transform.GetChild(1).GetChild(0).GetComponent<Button>().onClick.AddListener(pauseMenuFunc);
        gameObject.transform.GetChild(1).GetChild(1).GetComponent<Button>().onClick.AddListener(playerMenuFunc);

        pauseMenu.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(pauseMenuFunc);
        pauseMenu.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(Restart);
        pauseMenu.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(Quit);

        //Minus and Plus buttons for each stat
        playerMenu.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ModifyDefence(-1); });
        playerMenu.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { ModifyDefence(1); });

        playerMenu.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ModifyAttack(-1); });
        playerMenu.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { ModifyAttack(1); });

        playerMenu.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ModifyMovement(-1); });
        playerMenu.transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { ModifyMovement(1); });
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = playerHandler.GetTempHealth() / playerHandler.GetMaxHealth();

        if (playerHandler.GetHealth() <= 0)
            Restart();

        if (Input.GetButtonDown("Cancel"))
            pauseMenuFunc();
        if (Input.GetButtonDown("PlayerMenu"))
            playerMenuFunc();
    }

    void pauseMenuFunc()
    {
        isPauseMenu = !isPauseMenu;
        pauseMenu.SetActive(isPauseMenu);

        isPlayerMenu = false;
        playerMenu.SetActive(false);
    }
    void playerMenuFunc()
    {
        isPlayerMenu = !isPlayerMenu;
        playerMenu.SetActive(isPlayerMenu);

        isPauseMenu = false;
        pauseMenu.SetActive(false);
    }
    void Restart()
    {
        SceneManager.LoadScene(0);
    }
    void Quit()
    {
        Application.Quit();
    }

    public void ModifyDefence(short Modifier)
    {
        short PointTotal = playerHandler.GetPoints();
        short Total = (short)(playerHandler.GetDefence() + Modifier);
        if (Total < 0 || Modifier > PointTotal)
            return;

        playerHandler.ModifyDefence(Modifier);
        defenceCount.text = "" + Total;
        pointsCount.text = "" + (PointTotal - Modifier);

        if (Total < 10)
            defenceCount.text = "0" + defenceCount.text;
        if ((PointTotal - Modifier) < 10)
            pointsCount.text = "0" + pointsCount.text;
    }
    public void ModifyAttack(short Modifier)
    {
        short PointTotal = playerHandler.GetPoints();
        short Total = (short)(playerHandler.GetAttack() + Modifier);
        if (Total < 0 || Modifier > PointTotal)
            return;

        playerHandler.ModifyAttack(Modifier);
        attackCount.text = "" + Total;
        pointsCount.text = "" + (PointTotal - Modifier);

        if (Total < 10)
            attackCount.text = "0" + attackCount.text;
        if ((PointTotal - Modifier) < 10)
            pointsCount.text = "0" + pointsCount.text;
    }
    public void ModifyMovement(short Modifier)
    {
        short PointTotal = playerHandler.GetPoints();
        short Total = (short)(playerHandler.GetMovement() + Modifier);
        if (Total < 0 || Modifier > PointTotal)
            return;

        playerHandler.ModifyMovement(Modifier);
        movementCount.text = "" + Total;
        pointsCount.text = "" + (PointTotal - Modifier);

        if (Total < 10)
            movementCount.text = "0" + movementCount.text;
        if ((PointTotal - Modifier) < 10)
            pointsCount.text = "0" + pointsCount.text;
    }
}
