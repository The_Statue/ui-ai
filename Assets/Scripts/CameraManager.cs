﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    Vector3 startPos;
    GameObject target;
    GameObject player;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        startPos = transform.position;
        target = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(startPos, new Vector3(target.transform.position.x, target.transform.position.y, -10), timer);
        timer += Time.deltaTime * 0.5f;
    }
    public void UpdateTarget(GameObject followTarget)
    {
        timer = 0;
        startPos = transform.position;
        target = followTarget;
    }
}
