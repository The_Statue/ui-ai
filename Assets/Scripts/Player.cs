﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    public float health = 0;
    float tempHealth = 0;
    short maxHealth = 100;
    short defence = 0;
    short attack = 0;
    short movement = 0;
    short pointsRemaining = 20;

    //AI variables
    public Path pathfinder;
    public Grid grid;
    Vector3 originalPos;
    List<Node> path;
    float timer = 0;
    float animationSpeed = 10;

    public TurnManager turnManager;

    GameObject[] enemies;

    GameObject[] examplePathObjects = new GameObject[150];
    public GameObject examplePathPrefab;

    // Start is called before the first frame update
    void Start()
    {
        originalPos = Vector3.zero;
        health = maxHealth;

        for (int i = 0; i < examplePathObjects.Length; i++)
        {
            examplePathObjects[i] = Instantiate(examplePathPrefab);
        }

        enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        if (tempHealth > health)
            tempHealth -= (maxHealth * 1.25f) * Time.deltaTime;
        if (tempHealth < health)
            tempHealth += (maxHealth * 1.25f) * Time.deltaTime;

        if (health < 0)
            //death trigger
            health = 0;

        if (health > 100)
            health = 100;

        //Debug.Log("Player Health: " + health + " Player tempHealth: " + tempHealth);

        //Pathfinding
        if (pathfinder != null && (path == null || path.Count == 0) && turnManager.CheckTurn(gameObject) && (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()))
        {
            path = pathfinder.FindPath(gameObject.transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition));

            if (path.Count > movement)
            {
                Debug.Log("Trimmed path at " + movement + " by " + (path.Count - movement));
                path.RemoveRange(movement, path.Count - movement);
            }

            if (path.Count > 0)
            {
                //Wait and change to next turn/agent
                turnManager.IncrementTurn();
                Debug.Log("New Path Set!");
            }
            else
                Debug.LogError("Zero path!");

            if (path.Count > movement) //null turn if it is illegal
            {
                Debug.LogError(gameObject.name + " Illegal move", gameObject);
                path.Clear();
            }
        }

        //pathfinding animation
        if (path != null && path.Count > 0)
        {
            if (originalPos == Vector3.zero)
            {
                originalPos = gameObject.transform.position;
                timer = 0;
            }
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, path[0].worldPosition, 0.01f);
            if (Vector3.Distance(gameObject.transform.position, path[0].worldPosition) <= 0)
            {
                path.RemoveAt(0);
                originalPos = Vector3.zero;

                foreach(GameObject enemy in enemies) //Loop through all enemies, damage if they are on the final path node
                {
                    if (path.Count == 1 && Vector3.Distance(path[0].worldPosition, enemy.transform.position) <= 0.2f)
                    {
                        path.RemoveAt(0);
                        enemy.GetComponent<Enemy>().Damage(attack);
                    }
                }

                if (path.Count == 0)
                    turnManager.IncrementTurn();
            }
        }
        if (originalPos != Vector3.zero)
        {
            gameObject.transform.position = Vector3.Lerp(originalPos, path[0].worldPosition, timer);
            //Debug.Log("Pos: " + gameObject.transform.position + " " + originalPos + " " + path[0].worldPosition);
        }
        timer += Time.deltaTime * animationSpeed;

        //Draw path to mouse
        if (turnManager.CheckTurn(gameObject) && !EventSystem.current.IsPointerOverGameObject() && grid.NodeFromWorldPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition)).walkable)
        {
            List<Node> examplePath = pathfinder.FindPath(gameObject.transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            for (int i = 0; i < examplePathObjects.Length; i++)
            {
                if (i < examplePath.Count)
                {
                    examplePathObjects[i].transform.position = examplePath[i].worldPosition;
                    examplePathObjects[i].SetActive(true);
                }
                else
                    examplePathObjects[i].SetActive(false);
            }
        }
        else
            foreach (GameObject pathObject in examplePathObjects)
                pathObject.SetActive(false);
    }

    public float GetTempHealth()
    {
        return tempHealth;
    }
    public float GetHealth()
    {
        return health;
    }
    public short GetMaxHealth()
    {
        return maxHealth;
    }
    public void Damage(short modifier)
    {
        modifier *= 2;
        modifier -= defence;
        if (modifier <= 0)
            return;

        Debug.Log("Player took " + modifier + " damage");
        health -= modifier;
    }

    public void ModifyDefence(short Modifier)
    {
        defence += Modifier;
        ModifyPoints(Modifier);
    }
    public short GetDefence()
    {
        return defence;
    }

    public void ModifyAttack(short Modifier)
    {
        attack += Modifier;
        ModifyPoints(Modifier);
    }
    public short GetAttack()
    {
        return attack;
    }

    public void ModifyMovement(short Modifier)
    {
        movement += Modifier;
        ModifyPoints(Modifier);
    }
    public short GetMovement()
    {
        return movement;
    }

    void ModifyPoints(short Modifier)
    {
        pointsRemaining -= Modifier;
    }
    public short GetPoints()
    {
        return pointsRemaining;
    }
}
